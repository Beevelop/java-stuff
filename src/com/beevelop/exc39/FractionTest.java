package com.beevelop.exc39;

import com.beevelop.exc38.Fraction;

public class FractionTest {

    Fraction f1;
    Fraction f2;

    public FractionTest() {
        f1 = new Fraction();
        f2 = new Fraction();

        f1.read();
        f2.read();

        runTest();
    }

    private void runTest() {
        System.out.println("Mul: " + f1.mul(f2));

        System.out.println("Div: " + f1.div(f2));

        System.out.println("Add: " + f1.add(f2));

        System.out.println("Sub: " + f1.sub(f2));

        System.out.println("Val: " + f1.value());

        System.out.println("Rec: " + f1.reciprocal());

        System.out.println("Neg: " + f1.neg());

        System.out.println("Red: " + f1.reduce());
    }

    public static void main(String[] args) {
        new FractionTest();
    }
}
