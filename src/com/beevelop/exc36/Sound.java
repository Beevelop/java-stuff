package com.beevelop.exc36;

import java.lang.reflect.Field;

public class Sound {
    private String room;
    private int volume;
    private int bass;
    private int treble;

    public Sound(String room, int volume, int bass, int treble) {
        this.room = room;
        this.volume = volume;
        this.bass = bass;
        this.treble = treble;
    }

    public int getVolume() {
        return volume;
    }

    public void amplify(String controller, int value) throws NoSuchFieldException, IllegalAccessException {
        Field field = getClass().getDeclaredField(controller);
        field.setInt(this, field.getInt(this) + value);
    }

    @Override
    public String toString() {
        return String.format("Anlage im Raum %s: vol: %d, bas: %d, tre: %d", room, volume, bass, treble);
    }
}