package com.beevelop.exc36;

import com.beevelop.helper.BeeTools;

public class TestSound {

    public static void main(String[] args) {
        int volume, bass, treble;
        String room;
        Sound stereo;

        System.out.println("Daten fuer eine Anlage einlesen");
        room = BeeTools.readStr("Raum-Bezeichnung: ");
        volume = BeeTools.readInt("Standard-Einstellung fuer die Lautstaerke: ");
        bass = BeeTools.readInt("Standard-Einstellung fuer die Baesse: ");
        treble = BeeTools.readInt("Standard-Einstellung fuer die Hoehen: ");
        System.out.println();

        // Initialize stereo
        stereo = new Sound(room, volume, bass, treble);

        System.out.println("Daten der Anlage ausgeben");
        System.out.println(stereo);
        System.out.println();

        System.out.println("Die Anlage wird nun anders eingestellt");
        System.out.println("(Baesse um 4 erhoeht, Hoehen um 2 erniedrigt)");

        try {
            stereo.amplify("bass", 4);
            stereo.amplify("treble", -2);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        System.out.println();

        System.out.println("Daten der neu eingestellten Anlage ausgeben");
        System.out.println(stereo);
    }
}