package com.beevelop.exc40;

import com.beevelop.exc38.Fraction;

import javax.swing.*;

public class FractionCalculator {

    private Fraction f1;
    private Fraction f2;

    public FractionCalculator() {
        GUI gui = new GUI(this);

        f1 = new Fraction(1, 2);
        f2 = new Fraction(3, 4);
    }

    private String fractionToHtml(Fraction frac) {
        return String.format("<sup>%s</sup>&frasl;<sub>%s</sub>", frac.getNumerator(), frac.getDenominator());
    }

    public String mul() {
        Fraction res = f1.mul(f2);
        return String.format("%s &times; %s = %s", fractionToHtml(f1), fractionToHtml(f2), fractionToHtml(res));
    }

    public String reduce() {
        return String.format("%s = %s, %s = %s", fractionToHtml(f1), fractionToHtml(f1.reduce()), fractionToHtml(f2), fractionToHtml(f2.reduce()));
    }

    public String neg() {
        return String.format("%s => %s, %s => %s", fractionToHtml(f1), fractionToHtml(f1.neg()), fractionToHtml(f2), fractionToHtml(f2.neg()));
    }

    public String values() {
        return String.format("%s = %s, %s = %s", fractionToHtml(f1), f1.value(), fractionToHtml(f2), f2.value());
    }

    public String div() {
        return String.format("%s : %s = %s", fractionToHtml(f1), fractionToHtml(f2), fractionToHtml(f1.div(f2)));
    }

    public String add() {
        return String.format("%s + %s = %s", fractionToHtml(f1), fractionToHtml(f2), fractionToHtml(f1.add(f2)));
    }

    public String sub() {
        return String.format("%s - %s = %s", fractionToHtml(f1), fractionToHtml(f2), fractionToHtml(f1.sub(f2)));
    }

    public void updateFractions(int numerator1, int denominator1, int numerator2, int denominator2) {
        f1.setNumerator(numerator1);
        f1.setDenominator(denominator1);
        f2.setNumerator(numerator2);
        f2.setDenominator(denominator2);
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        new FractionCalculator();
    }
}
