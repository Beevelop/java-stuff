package com.beevelop.exc40;

import com.sun.deploy.util.StringUtils;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class GUI {
    private JSpinner numerator1;
    private JSpinner denominator1;
    private JPanel mainPanel;
    private JTextPane logTxt;
    private JButton reduceBtn;
    private JButton negBtn;
    private JButton valuesBtn;
    private JButton mulBtn;
    private JButton divBtn;
    private JButton addBtn;
    private JButton subBtn;
    private JSpinner numerator2;
    private JSpinner denominator2;
    private JPanel operationsPanel;

    private ArrayList<String> logs;

    public GUI(final FractionCalculator frac) {
        logs = new ArrayList<String>();

        JFrame frame = new JFrame("GUI");
        frame.setContentPane(this.mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);

        numerator1.setValue(1);
        denominator1.setValue(2);
        numerator2.setValue(3);
        denominator2.setValue(4);

        mulBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                log(frac.mul());
            }
        });
        reduceBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                log(frac.reduce());
            }
        });
        negBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                log(frac.neg());
            }
        });
        valuesBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                log(frac.values());
            }
        });
        divBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                log(frac.div());
            }
        });
        addBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                log(frac.add());
            }
        });
        subBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                log(frac.sub());
            }
        });

        ChangeListener onFractionChange = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if ((Integer) denominator1.getValue() == 0 || (Integer) denominator2.getValue() == 0) {
                    disableBtns();
                    return;
                }
                frac.updateFractions((Integer) numerator1.getValue(), (Integer) denominator1.getValue(), (Integer) numerator2.getValue(), (Integer) denominator2.getValue());
            }
        };

        numerator1.addChangeListener(onFractionChange);
        denominator1.addChangeListener(onFractionChange);
        numerator2.addChangeListener(onFractionChange);
        denominator2.addChangeListener(onFractionChange);
    }

    private void disableBtns() {
        operationsPanel.setVisible(false);
        log("Zero sucks!");
    }

    private void log(String msg) {
        logs.add(msg);
        logTxt.setText(StringUtils.join(logs, "<br>"));
    }
}
