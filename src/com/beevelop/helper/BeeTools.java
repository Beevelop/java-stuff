package com.beevelop.helper;

import java.util.Scanner;

public class BeeTools {

    private static Scanner sc;

    private static Scanner scan() {
        if (sc == null) {
            sc = new Scanner(System.in);
        }
        return sc;
    }

    public static String readStr(String msg) {
        System.out.print(msg);
        return scan().nextLine();
    }

    public static int readInt(String msg) {
        System.out.print(msg);
        int i = scan().nextInt();
        scan().nextLine();
        return i;
    }


}
