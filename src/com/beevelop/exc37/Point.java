package com.beevelop.exc37;

import com.beevelop.helper.BeeTools;

public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void read() {
        this.x = BeeTools.readInt("X: ");
        this.y = BeeTools.readInt("Y: ");
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
