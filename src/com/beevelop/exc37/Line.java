package com.beevelop.exc37;

public class Line {
    private Point p;
    private Point q;

    public Line(Point p, Point q) {
        this.p = p;
        this.q = q;
    }

    public void read() {
        p.read();
        q.read();
    }

    public double getLength() {
        double x = Math.pow(p.getX() - q.getX(), 2);
        double y = Math.pow(p.getY() - q.getY(), 2);
        return Math.sqrt(x + y);
    }

    @Override
    public String toString() {
        return "Line{" +
                "p=" + p +
                ", q=" + q +
                '}';
    }
}
