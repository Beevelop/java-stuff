package com.beevelop.exc37;

public class TestLine {
    public static void main(String[] args) {
        Point origin = new Point(0.0, 0.0);
        Point endpoint = new Point(4.0, 3.0);
        Line s = new Line(origin, endpoint);
        System.out.println("Die Laenge der Strecke " + s +
                " betraegt " + s.getLength() + ".");
        System.out.println();

        System.out.println("Strecke s eingeben:");
        s.read();
        System.out.println();

        System.out.println("Die Laenge der Strecke " + s +
                " betraegt " + s.getLength() + ".");
    }
}

