package com.beevelop.exc38;

import com.beevelop.helper.BeeTools;

public class Fraction {

    public static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }

    private int numerator;
    private int denominator;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction() {
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

    public Fraction reduce() {
        int divisor = Math.abs(gcd(numerator, denominator));
        return new Fraction(numerator / divisor, denominator / divisor);
    }

    public void read() {
        numerator = BeeTools.readInt("Numerator: ");

        denominator = 0;
        do {
            denominator = BeeTools.readInt("Denominator: ");
        } while (denominator == 0);
    }

    public Fraction reciprocal() {
        return new Fraction(denominator, numerator);
    }

    public Fraction neg() {
        return new Fraction(-numerator, denominator);
    }

    public double value() {
        return (double) numerator / (double) denominator;
    }

    public Fraction mul(Fraction f) {
        return new Fraction(numerator * f.getNumerator(), denominator * f.getDenominator()).reduce();
    }

    public Fraction div(Fraction f) {
        f = f.reciprocal();
        return new Fraction(numerator * f.getNumerator(), denominator * f.getDenominator()).reduce();
    }

    public Fraction add(Fraction f) {
        int newNumerator = ((f.getNumerator() * denominator) + (numerator * f.getDenominator()));
        int newDenominator = denominator * f.getDenominator();
        Fraction res = new Fraction(newNumerator, newDenominator);
        return res.reduce();
    }

    public Fraction sub(Fraction f) {
        int newNumerator = ((numerator * f.getDenominator()) - (f.getNumerator() * denominator));
        int newDenominator = denominator * f.getDenominator();
        Fraction res = new Fraction(newNumerator, newDenominator);
        return res.reduce();
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }
}
